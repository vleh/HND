var createError = require('http-errors');
var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var cors = require('cors');

var passport = require('passport');
var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;
var jwt = require('jsonwebtoken');

/*
var index = require('./routes/index');
var users = require('./routes/users');
*/

var dbUrl= "mongodb://valentin:valentin@ds119060.mlab.com:19060/gamesapi";
var db = require('monk')(dbUrl);

const gamesapi = db.get('gamesapi');
const users = db.get('users');  

var opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme("jwt");
opts.secretOrKey = "Vleh";

passport.use(new JwtStrategy(opts, function (jwt_payload, done) {
    users.findOne({"_id": jwt_payload._id})
        .then(function (user) {
            if (user){
                done(null, user)
            }else{
                done(null,false)
            }
        }).catch(function (error) {
            console.log("Error" + error);
    });
}));

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'app/json' }));


app.use(cors());


/*
app.use('/', indexRouter);
app.use('/users', usersRouter);
*/

app.post('/api/auth/register', function (req, res) {
    var data =req.body;

    if (!data.username || !data.password) {
        res.json({success: false, msg: 'Please pass username and password.'});
    }else {
        users.findOne({username: data.username})
            .then(function (user) {
                console.log(user);
                if (!user){
                    users.insert(data);
                    return res.json({success: true, msg: 'User created.'});
                }else{
                    return res.json({success: false, msg: 'Username already exist.'})
                }
            });
    }
});

app.post('/api/auth/login', function (req,res) {

    var data = req.body;

    users.findOne({username: data.username})
        .then(function (user) {
            console.log(user);
            if (!user){
                res.status(401).send({success: false, msg: 'Authentication failed. User not found.'});
            }else {
                if (user.password == data.password){
                    var token = jwt.sign(
                        {
                            "_id": user._id,
                            "username": user.username
                        },
                        opts.secretOrKey
                    );
                    res.json({success: true, token: 'JWT' + token});
                }else {
                    res.status(401).send({success: false, msg: 'Authentication failed. Wrong password.'})
                }
            }
        })
});

app.get('/api/heroes', function (req, res) {
    gamesapi.find({})
        .then(function (data) {
            res.json({heroes: data});
        }).catch(function (error){
        console.log(error);
    });
});

app.post('/api/heroes', function (req, res) {
    var data = req.body;

    gamesapi.insert(data)
        .then(function (data) {
            res.json({heroes: data});
        }).catch(function (error){
        console.log(error);
    });
});

app.get('/api/heroes/:id', function (req, res) {
    var _id = req.params.id;

    gamesapi.find({"_id": _id})
        .then(function (data) {
            res.json(data);
        }).catch(function (error){
        console.log(error);
    });
});

app.post('/api/heroes/:id', function (req, res) {
    var _id = req.params.id;
    var data = req.body;

    gamesapi.update({"_id": _id}, data)
        .then(function (data) {
            res.json(data);
        }).catch(function (error){
        console.log(error);
    });
});

app.delete('/api/heroes/:id', passport.authenticate('jwt', {session: false}), function (req, res) {
    if (req.user){
        var _id = req.params.id;
        console.log(req.user.username);

        console.log(_id);
        gamesapi.remove({"_id": _id})
            .then(function (data) {
                console.log('WOW funca');

                res.json(data);
            }).catch(function (error){
            console.log('Error ' + error);
        });
    }else{
        return res.status(403).send({success: false, msg: 'Unauthorized'})
    }
});


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
